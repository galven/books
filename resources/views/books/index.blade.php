<!DOCTYPE html>
<html>
@yield('content')
@extends('layouts.app')
@section('content') 
<head><h3>This is your Book List</h3></head>
<body>
<table>
    <tr>
     <th>ID</th>
     <th>Book Title</th>
     <th>Book Author</th>
     <th>Read The Book?</th>
     <th>Update The Book</th>
    </tr>

    @foreach($books as $book)
    <tr> 
        <td>{{$book->id}}</td>
      <td> {{$book->title}} </a></td>
      <td> {{$book->author}} </a></td>
      <td> @if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif </td>
      <td> @can('teacher') <a href = "{{route('books.edit',$book->id)}}">update</a>@endcan</td>
   </tr>
   @endforeach
</table>
@can('teacher')<a href = "{{route('books.create')}}"> create a new book </a>@endcan
<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url:"{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put',
                   contentType: 'application/json',
                   data:JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function(data){
                        console.log(JSON.stringify(data));
                   },
                   error: function(errorThrown){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  
@endsection
</html>