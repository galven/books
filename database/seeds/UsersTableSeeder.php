<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                'name'=> 'Moshe',
                'email'=>'moshe@gmail.com',
                'password' => '555555',
                'created_at' => date('y-m-d G:i:s'),
                ],
                [
                'name'=> 'Hadar',
                'email'=>'hadar@gmail.com',
                'password' => '12345678',
                'created_at' => date('y-m-d G:i:s'),
                ],
            ]
    );
    }
}
