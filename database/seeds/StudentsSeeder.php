<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class StudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                'name'=> 'Rotem',
                'email'=>'Rotem@gmail.com',
                'password' =>  Hash::make('12345678'),
                'created_at' => date('y-m-d G:i:s'),
                ],
                [
                'name'=> 'Nataly',
                'email'=>'Nataly@gmail.com',
                'password' =>  Hash::make('12345678'),
                'created_at' => date('y-m-d G:i:s'),
                ],
            ]
    );
    }

}
