<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books') ->insert(
            [   
                [
                    'title' => 'maasae bechmish balonim',
                    'author' => 'Miriam ruth',
                    'user_id' => '1',
                    'created_at' => date('Y-m-d G:i:s'),       
                    
                ],
                [
                    'title' => 'Haria Sahav Tot',
                    'author' => 'Tirza Atar',
                    'user_id' => '1',
                    'created_at' => date('Y-m-d G:i:s'),
                  
                ],
                [
                    'title' => 'Tiras Ham',
                    'author' => 'Tirza Atar',
                    'user_id' => '2',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'title' => 'Dira Lhascir',
                    'author' => 'Lea Goldberg',
                    'user_id' => '2',
                    'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'title' => 'Zehava vesloshet Hadobim',
                    'author' => 'Robert Sarti',
                    'user_id' => '2',
                    'created_at' => date('Y-m-d G:i:s'),
                ]
            ]);
    }
}
