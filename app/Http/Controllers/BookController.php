<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBookPostRequest;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $books = Book::all();
        $id = Auth::id(); // we will use 2 later
        if (Gate::denies('teacher')) {
            $boss = DB::table('students')->where('student',$id)->first();
            $id = $boss->teacher;
        } 
        $user = User::find($id);
        $books = $user->books;
        return view('books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('teacher')) {
            abort(403,"Sorry you are not allowed to create books..");
        }
 
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookPostRequest $request)
    {
        if (Gate::denies('teacher')) {
            abort(403,"Are you a hacker or what?");
       } 

       $validated = $request->validated();

        $books = new Book();
        $id = Auth::id(); // יצירת ספר חדש
      //  $id = Auth::id();
        $books->title = $request->title;
        $books->user_id = $id;
        $books->author = $request->author;
        $books->status = 0;
        $books-> save();
        return redirect('books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('teacher')) {
            abort(403,"Are you a hacker or what?");
       } 
        $book = Book::find($id);
       return view('books.edit', ['book' => $book]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::findOrFail($id);
        if (Gate::denies('teacher')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit books..");
        }   
 
       // $book->update($request -> all());
    if (Gate::allows('teacher')){   // בדיקה שהמורה רוצה לסמן וי או להוריד וי מהצ'קבוקס
        if(!$book->user->id == Auth::id()) return(redirect('books')); //בדיקה שאכן מי שמחובר לסשן הוא זה שמבצע עדכון
         $book->update($request-> except(['_token'])); // עדכון הצ'קבוקס
         if($request->ajax()){
             return Response::json(array('result' =>'sucess','status' =>$request->status),200);
         }
     }
     if (Gate::allows('student')){ // בדיקה שסטודנט רוצה לעדכן את הצ'קבוקס
         if ($book->status == 0){ // אם הצ'קבוקס כרגע לא מסומן בוי
             if(!$book->user->id == Auth::id()) return(redirect('books')); // בדיקה שאכן מי שמחובר לסשן הוא זה שמבצע עדכון
         $book->update($request-> except(['_token'])); // עדכון הצ'קבוקס
                   if($request->ajax()){
                  return Response::json(array('result' =>'sucess','status' =>$request->status),200);
                  }
         }
     }
        return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('teacher')) {
            abort(403,"Are you a hacker or what?");
       } 
        $book = Book::find($id);
        $book->delete(); 
        return redirect('books');
    }
}
